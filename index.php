<?php
require_once __DIR__ . '/config.php';

$loader = require_once __DIR__ . '/vendor/autoload.php';
$loader->add('StNicolasDay\\', __DIR__ . '/src/StNicolasDay');

$randomMachine = new StNicolasDay\RandomMachine(SMPT_USER, SMPT_PASSWORD, SMPT_HOST, SMPT_PORT, SMPT_ENCRYPTION);

$randomMachine->run();
