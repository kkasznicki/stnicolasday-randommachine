<?php
namespace StNicolasDay;

class RandomMachine
{

    private static $participants = array(
        'adam.miazio@incitosystems.com',
        'marcin@incitosystems.com',
        'marek.gerwel@incitosystems.com',
        'pawel.bodo@incitosystems.com',
        'karol.kasznicki@incitosystems.com',
        'pawel.gawlicki@incitosystems.com',
        'mariusz.szymczyk@incitosystems.com',
        'marcin.drazek@incitosystems.com',
    );
    private static $pairs = array();
    private $smtpHost;
    private $smtpPort;
    private $smtpUser;
    private $smtpPassword;

    public function __construct($smtpUser, $smtpPassword, $smtpHost, $smtpPort, $smtpEncription)
    {
        $this->smtpHost = $smtpHost;
        $this->smtpPort = $smtpPort;
        $this->smtpUser = $smtpUser;
        $this->smtpPassword = $smtpPassword;
        $this->smtpEncription = $smtpEncription;
    }

    public function run()
    {
        $first = $ele1 = array_shift(self::$participants);
        while (count(self::$participants)) {
            // get random element
            $ele2 = array_rand(self::$participants);
            // associate elements
            self::$pairs[$ele1] = self::$participants[$ele2];
            // random element becomes next element
            $ele1 = self::$participants[$ele2];
            // delete the random element
            array_splice(self::$participants, $ele2, 1);
        }
        // associate last element with the first one
        self::$pairs[$ele1] = $first;

        foreach (self::$pairs as $recipient => $pairEmail) {
            $isSend = $this->sendNotification($recipient, $pairEmail);

            echo ($isSend ? 'send email to: ' . $recipient : 'email not sent');
        }
    }

    private function sendNotification($recipient, $pairEmail)
    {
        // Create the Transport
        $swiftTransport = new \Swift_SmtpTransport($this->smtpHost, $this->smtpPort, $this->smtpEncription);
        $transport = $swiftTransport
            ->setUsername($this->smtpUser)
            ->setPassword($this->smtpPassword);

        // Create the Mailer using your created Transport
        $mailer = new \Swift_Mailer($transport);

        $body = "Twoja para to: " . $pairEmail . "\r\n\r\n" .
            "Zasady:\r\n" .
            "1. zakup prezentu za nie mniej niż 40 PLN" . "\r\n" .
            "2. zakaz przekazywania informacji o osobie wylosowanej";

        // Create a message
        $swiftMessage = new \Swift_Message('Mikolajkowy random machine');
        $message = $swiftMessage
            ->setFrom(array('karol.kasznicki@incitosystems.com' => 'Karol Kasznicki'))
            ->setTo(array($recipient))
            ->setBody($body, 'text/plain');

        // Send the message
        $result = $mailer->send($message);

        return $result;
    }
}
